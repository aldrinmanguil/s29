db.users.insertMany([
	{
		"firstName": "Stephen",
		"lastName": "Hawking",
		"age": 76,
		"email": "stephenhawking@mail.com",
		"department": "HR"
	},
		{
		"firstName": "Niel",
		"lastName": "Armstrong",
		"age": 82,
		"email": "nielarmstrong@mail.com",
		"department": "HR"
	},
	{
		"firstName": "Bill",
		"lastName": "Gates",
		"age": 65,
		"email": "billgates@mail.com",
		"department": "Operations"
	},
	{
		"firstName": "John",
		"lastName": "Doe",
		"age": 21,
		"email": "johndoe@mail.com",
		"department": "HR"
	}
])

db.users.find({
	$or: [
		{"firstName": {$regex: 's', $options:'$i'}},
		{"lastName": {$regex: 'd', $options:'$i'}} 
		]},
		{"_id": 0,
		"firstName": 1, "lastName": 1}
	
)

db.users.find({
	$and: [
	{"department": "HR"},{ "age": {$gte: 70}}
	]
})


db.users.find({
	$and: [
	{"firstName": {$regex: 's', $options: $i}},
	{"age": {$lte: 30}}
	]
})